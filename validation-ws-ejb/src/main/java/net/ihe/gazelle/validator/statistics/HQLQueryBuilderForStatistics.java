package net.ihe.gazelle.validator.statistics;

import java.util.Date;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.beans.HQLOrder;
import net.ihe.gazelle.hql.beans.HQLRestrictionValues;

/**
 * <p>HQLQueryBuilderForStatistics class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HQLQueryBuilderForStatistics extends HQLQueryBuilder<ValidatorUsage>{

	/**
	 * <p>Constructor for HQLQueryBuilderForStatistics.</p>
	 */
	public HQLQueryBuilderForStatistics() {
		super(ValidatorUsage.class);
	}
	
	/**
	 * <p>getMinDate.</p>
	 *
	 * @return a {@link java.util.Date} object.
	 */
	public Date getMinDate()
	{
		StringBuilder sb = new StringBuilder("select min(date) ");
		// First build where, to get all paths in from
		StringBuilder sbWhere = new StringBuilder();
		HQLRestrictionValues values = buildQueryWhere(sbWhere);
		// Build order, to get all paths in from
		buildQueryOrder(sbWhere);

		// add order columns if needed, cannot sort without it!
		// will be removed at the end (result list will be Object[] then)
		for (HQLOrder order : getOrders()) {
			sb.append(", ").append(order.getPath());
		}
		sb.append(buildQueryFrom());
		sb.append(sbWhere);

		Query query = createRealQuery(sb);

		buildQueryWhereParameters(query, values);
		try{
			return (Date) query.getSingleResult();
		}catch(NoResultException e){
			e.printStackTrace();
			return null;
		}
	}
}
