package net.ihe.gazelle.validator.statistics;

import net.ihe.gazelle.validator.validation.model.ValidatorUsageProvider;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;

@Named("validatorUsageProvider")
public class ValidatorUsageProviderImpl implements ValidatorUsageProvider{

    @Inject
    private ValidatorUsageDAO validatorUsageDAO;

    /** {@inheritDoc} */
    @Override
    public void newEntry(Date date, String status, String type, String caller) {
        ValidatorUsage usage = new ValidatorUsage(date, status, type, caller);
        validatorUsageDAO.save(usage);
    }
}
