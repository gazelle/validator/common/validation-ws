package net.ihe.gazelle.validator.statistics;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <b>Class description</b>: ValidatorUsage
 *
 * This class is used to be able to report statistics on the usage of the validator embedded in the validator
 *
 * @author 	Anne-Gaëlle Bergé / IHE Europe
 */

@Entity
@Table(name="cmn_validator_usage", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="cmn_validator_usage_sequence", sequenceName="cmn_validator_usage_id_seq", allocationSize=1)
public class ValidatorUsage implements Serializable{

	private static final long serialVersionUID = -6685024671308605414L;

	@Id
	@GeneratedValue(generator="cmn_validator_usage_sequence", strategy=GenerationType.SEQUENCE)
	@NotNull
	@Column(name="id", nullable=false, unique=true)
	private Integer id;
	
	@Column(name="date")
	@Temporal(TemporalType.DATE)
	private Date date;
	
	@Column(name="status")
	private String status;
	
	@Column(name="type")
	private String type;
	
	@Column(name="caller")
	private String caller;
	
	/**
	 * <p>Constructor for ValidatorUsage.</p>
	 */
	public ValidatorUsage() {}
	
	/**
	 * <p>Constructor for ValidatorUsage.</p>
	 *
	 * @param validationDate a {@link java.util.Date} object.
	 * @param validationStatus a {@link java.lang.String} object.
	 * @param validationType a {@link java.lang.String} object.
	 * @param caller a {@link java.lang.String} object.
	 */
	public ValidatorUsage(Date validationDate, String validationStatus, String validationType, String caller) {
		this.date = validationDate;
		this.status = validationStatus;
		this.caller = caller;
		this.type = validationType;
	}
	
	/**
	 * <p>Setter for the field <code>dates</code>.</p>
	 *
	 * @param date a {@link java.util.Date} object.
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * <p>Setter for the field <code>status</code>.</p>
	 *
	 * @param status a {@link java.lang.String} object.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * <p>Setter for the field <code>type</code>.</p>
	 *
	 * @param type a {@link java.lang.String} object.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * <p>Setter for the field <code>caller</code>.</p>
	 *
	 * @param caller a {@link java.lang.String} object.
	 */
	public void setCaller(String caller) {
		this.caller = caller;
	}
}
