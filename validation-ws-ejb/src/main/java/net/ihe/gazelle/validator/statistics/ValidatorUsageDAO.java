package net.ihe.gazelle.validator.statistics;

public interface ValidatorUsageDAO {

    void save(ValidatorUsage validatorUsage);

}
