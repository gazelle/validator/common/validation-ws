package net.ihe.gazelle.validator.validation.model;

import java.util.Date;

public interface ValidatorUsageProvider {
	
	public void newEntry(Date date, String status, String type, String caller);

}
