package net.ihe.gazelle.validator.validation.model;


public interface ValidatorDescription {

	String getOid();
	
	String getName();
	
	String getDescriminator();

	String getRootElement();

	String getNamespaceURI();

	boolean extractPartToValidate();
}
