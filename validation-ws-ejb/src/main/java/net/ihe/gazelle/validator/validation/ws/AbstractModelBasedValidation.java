package net.ihe.gazelle.validator.validation.ws;

import net.ihe.gazelle.validator.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validator.validation.model.ValidatorDescription;
import net.ihe.gazelle.validator.validation.model.ValidatorUsageService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public abstract class AbstractModelBasedValidation implements ModelBasedValidationRemote {

    @Resource
    WebServiceContext wsCtx;

    private boolean validatingSubPart;

    @WebMethod
    @WebResult(name = "about")
    public String about() {
        String disclaimer = "This webservice is developped by IHE-europe / gazelle team. "
                + "The aim of this validator is to validate XML documents using model based validation.\n";
        disclaimer = disclaimer
                + "For more information please contact the manager of the Gazelle project: eric.poiseau@inria.fr";
        return disclaimer;
    }

    @WebMethod
    @WebResult(name = "Validators")
    public List<String> getListOfValidators(@WebParam(name = "descriminator") String descriminator) throws SOAPException {
        List<ValidatorDescription> validators = getValidatorsForDescriminator(descriminator);
        if (validators == null || validators.isEmpty()) {
            throw new SOAPException("No validator is available for descriminator " + descriminator);
        } else {
            List<String> values = new ArrayList<String>();
            for (ValidatorDescription desc : validators) {
                values.add(desc.getName());
            }
            Collections.sort(values);
            return values;
        }
    }

    @WebMethod
    @WebResult(name = "DetailedResult")
    public String validateBase64Document(@WebParam(name = "base64Document") String base64Document,
                                         @WebParam(name = "validator") String validator) throws SOAPException {
        if (validator == null || validator.isEmpty()) {
            throw new SOAPException("A validator must be provided");
        } else if (base64Document == null || base64Document.isEmpty()) {
            throw new SOAPException("The document to validate is empty and should not");
        } else {
            byte[] buffer = DatatypeConverter.parseBase64Binary(base64Document);
            if (buffer != null) {
                String document = new String(buffer);
                return this.validateDocument(document, validator);
            } else {
               throw new SOAPException("Cannot decode Base64 string");
            }
        }
    }

    @WebMethod
    @WebResult(name = "DetailedResult")
    public String validateDocument(@WebParam(name = "document") String document,
                                   @WebParam(name = "validator") String validator) throws SOAPException {
        if (validator == null || validator.isEmpty()) {
            throw new SOAPException("A validator must be provided");
        } else if (document == null || document.isEmpty()) {
            throw new SOAPException("The document to validate is empty and should not");
        } else {
            ValidatorDescription selectedValidator = getValidatorByOidOrName(validator);
            String result;
            if (selectedValidator != null) {
                try {
                    String documentToValidate = document;
                    if (selectedValidator.extractPartToValidate()) {
                        documentToValidate = extractPartFromDocument(document, selectedValidator);
                    } else {
                        validatingSubPart = false;
                    }
                    result = executeValidation(documentToValidate, selectedValidator, validatingSubPart);
                } catch (GazelleValidationException e) {
                    result = buildReportOnParsingFailure(e, selectedValidator);
                }
            } else {
                throw new SOAPException("No validator found for parameter " + validator);
            }
            return result;
        }
    }

    protected abstract String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription selectedValidator);

    /**
     * The goal of this method is to extract the actual document to validate
     *
     * @param document
     * @param validator
     *
     * @return
     */
    private String extractPartFromDocument(String document, ValidatorDescription validator) throws GazelleValidationException {
        try {
            Document xmlDocument = createDocumentFromString(document);
            Element root = xmlDocument.getDocumentElement();
            String localName = root.getLocalName();
            if (validator.getRootElement().equals(localName)) {
                validatingSubPart = false;
                return document;
            }
            NodeList nodes = root.getElementsByTagNameNS(validator.getNamespaceURI(), validator.getRootElement());
            if (nodes.getLength() > 0) {
                Node selectedNode = nodes.item(0);
                validatingSubPart = true;
                return nodeToString(selectedNode);
            } else {
                throw new GazelleValidationException("Cannot find node with name " + validator.getNamespaceURI() + ":" + validator.getRootElement());
            }
        } catch (ParserConfigurationException e) {
            throw new GazelleValidationException("Cannot create factory", e);
        } catch (SAXException | IOException e) {
            throw new GazelleValidationException("Cannot parse document to extract part to validate", e);
        } catch (TransformerException e) {
            throw new GazelleValidationException("Cannot extract part to validate", e);
        }
    }

    private String nodeToString(Node node) throws TransformerException {
        StringWriter sw = new StringWriter();
        Transformer t = TransformerFactory.newInstance().newTransformer();
        t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.transform(new DOMSource(node), new StreamResult(sw));
        return sw.toString();
    }

    protected abstract String executeValidation(String document, ValidatorDescription validator, boolean extracted) throws GazelleValidationException;

    protected void addValidatorUsage(String validator, String status) {
        HttpServletRequest hRequest = (HttpServletRequest) wsCtx.getMessageContext()
                .get(MessageContext.SERVLET_REQUEST);
        String callerIp = hRequest.getRemoteAddr();
        ValidatorUsageService.newEntry(new Date(), status, validator, callerIp);
    }

    protected abstract ValidatorDescription getValidatorByOidOrName(String value);

    protected abstract List<ValidatorDescription> getValidatorsForDescriminator(String descriminator);

    private Document createDocumentFromString(String messageContent) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new ByteArrayInputStream(messageContent.getBytes(StandardCharsets.UTF_8)));
    }
}
