package net.ihe.gazelle.validator.validation.model;

import java.util.Date;

import net.ihe.gazelle.services.GenericServiceLoader;

import javax.enterprise.inject.spi.CDI;


public class ValidatorUsageService{
	
	private ValidatorUsageService(){
		
	}
	
	private static ValidatorUsageProvider getProvider(){
		return CDI.current().select(ValidatorUsageProvider.class).get();
	}
	
	public static void newEntry(Date date, String status, String type, String caller){
		getProvider().newEntry(date, status, type, caller);
	}
}
