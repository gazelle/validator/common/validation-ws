package net.ihe.gazelle.validator.validation.exception;

/**
 * <p>GazelleValidationException class.</p>
 *
 * @author abe
 * @version 1.0: 26/02/18
 */

public class GazelleValidationException extends Exception {

    public GazelleValidationException(){
        super();
    }

    public GazelleValidationException(String message, Exception cause){
        super(message, cause);
    }

    public GazelleValidationException(String message){
        super(message);
    }
}
