package net.ihe.gazelle.validator.statistics;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named("validatorUsageDAO")
public class ValidatorUsageDAOImpl implements ValidatorUsageDAO {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * <p>save.</p>
     */
    public void save(ValidatorUsage validatorUsage) {
        entityManager.merge(validatorUsage);
        entityManager.flush();
    }
}
