package net.ihe.gazelle.validator.validation.ws;

import java.util.List;

import javax.ejb.Remote;
import javax.xml.soap.SOAPException;

@Remote
public interface ModelBasedValidationRemote {
	
	/**
	 * Gives information about the called web service
	 * @return
	 */
	public String about();
	
	/**
	 * Validates an XML document using the given model-based validator
	 * @param document
	 * @param validator
	 * @return
	 * @throws SOAPException
	 */
	public String validateDocument(String document, String validator) throws SOAPException;
	
	/**
	 * Validates an XML document using the given model-based validator
	 * @param base64Document: base64 encoded document
	 * @param validator
	 * @return
	 * @throws SOAPException
	 */
	public String validateBase64Document(String base64Document, String validator) throws SOAPException;
	
	/**
	 * Returns the list of available validators
	 * @param descriminator: in some cases we may need to use the descriminator in order to select the validators to return
	 * @return
	 * @throws SOAPException
	 */
	public List<String> getListOfValidators(String descriminator) throws SOAPException;
	
}
